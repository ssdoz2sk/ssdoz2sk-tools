const Router = require('koa-router')

const view = require('../views/xls-tool')

const router = new Router({ prefix: '/xls'})

router
  .post('/toCsv', view.uploadFileToCSV)
  .post('/toXlsx', view.uploadFileToXLSX)

module.exports = router