const combineRouters = require('koa-combine-routers')
const xlsRouters = require('./xlsRouter')

const router = combineRouters(
  xlsRouters
)

module.exports = router