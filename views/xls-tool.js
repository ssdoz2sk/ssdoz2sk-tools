const fs = require('fs')
const path = require('path')
const XLSX = require('xlsx')

const view = {}

view.uploadFileToCSV = async (ctx, next) => {
  const xlsFile = ctx.request.files.xls
  
  if (xlsFile.type !== 'application/vnd.ms-excel' &&
      xlsFile.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
    return ctx.throw(400, `MIME type ('text/html') is not a supported stylesheet`)
  }

  const workbook = XLSX.readFile(xlsFile.path)
  const sheetNames = workbook.SheetNames
  const sheetName = sheetNames[0]
  const sheet = workbook.Sheets[sheetName]


  ctx.set({
    'Content-Type': 'application/octet-stream',
    'Content-Disposition': `attachment; filename=${xlsFile.hash}.csv`
  })
  return ctx.body = XLSX.stream.to_csv(sheet)
}

view.uploadFileToXLSX = async (ctx, next) => {
  const xlsFile = ctx.request.files.xls
  
  if (xlsFile.type !== 'application/vnd.ms-excel' &&
      xlsFile.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
    return ctx.throw(400, `MIME type ('text/html') is not a supported stylesheet`)
  }

  const workbook = XLSX.readFile(xlsFile.path)
  const filepath = path.join(TMP_DIR_PATH, `${xlsFile.hash}.xlsx`)
  
  XLSX.writeFile(workbook, filepath)
  const stream = fs.createReadStream(filepath)

  ctx.set({
    'Content-Type': 'application/octet-stream',
    'Content-Disposition': `attachment; filename=${xlsFile.hash}.xlsx`
  })
  return ctx.body = stream
}

module.exports = view