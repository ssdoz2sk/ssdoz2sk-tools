const path = require('path')
const os = require('os')
const fs = require('fs')

const Koa = require('koa')
const koaBody = require('koa-body')
const router = require('./routers')
const log4js = require('log4js')

const app = new Koa()

// 從環境變數讀取 SECRET_KEY 供 session 與 忘記密碼 的 token 產生使用，長度必須為 32 bytes
if(!process.env.SECRET_KEY) {
  process.env.SECRET_KEY = [...Array(32)].map(i=>(~~(Math.random()*36)).toString(36)).join('')
}
const key = process.env.SECRET_KEY

app.keys = [key]

// 把專案資料夾、static、暫存檔案位置寫入全域變數
global.BASE_PATH = __dirname
global.TMP_DIR_PATH = path.join(os.tmpdir(), 'ssdoz2sk-tools')

// 確認資料夾是否存在並產生
if (!fs.existsSync(TMP_DIR_PATH)){
  fs.mkdirSync(TMP_DIR_PATH)
}

// 設定紀錄檔案
log4js.configure({
  appenders: {
    http: { type: 'dateFile', filename: 'logs/http.log', pattern: '.yyyy-MM-dd', keepFileExt: true, compress: true },
    access: { type: 'dateFile', filename: 'logs/access.log', pattern: '.yyyy-MM-dd', keepFileExt: true, compress: true },
    out: { type: 'stdout' }
  },
  categories: {
    default: { appenders: ['http', 'out'], level: 'info' },
    access: { appenders: ['access'], level: 'info' }
  }
})

// 寫入存取紀錄
app.use(async (ctx, next) => {
  const logger = log4js.getLogger('access')
  logger.info(`${ctx.request.ip} - ${ctx.method} ${ctx.path}`, { header: ctx.request.header })
  await next()
})

// 把 logger 附到 ctx 中，並接取整個系統的錯誤寫入紀錄檔案
app.use(async (ctx, next) => {
  ctx.logger = log4js.getLogger('http')
  try {
    await next()
  } catch ( err ) {
    ctx.logger.error({ err })
    throw err
  }
})

// rewrite ctx.throw
app.use(async (ctx, next) => {
  ctx._throw = ctx.throw
  ctx.throw = function(status, message) {
    ctx.status = status
    ctx.body = message
  }
  
  await next()
})

// 設定檔案上傳參數
app.use(koaBody({
  multipart: true,
  formLimit: '20mb',
  formidable:{
    uploadDir: TMP_DIR_PATH,
    keepExtensions: true,
    hash: 'sha1',
    maxFieldsSize: 20 * 1024 * 1024 // 20MB
  }
}))

// 初始化網站路由
app.use(router())

module.exports = app
