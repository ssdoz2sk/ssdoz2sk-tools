FROM node:10

ENV PORT=80

WORKDIR /ssdoz2sk-tools

COPY package.json yarn.lock /ssdoz2sk-tools/
RUN yarn --production && yarn cache clean --force 

COPY . /ssdoz2sk-tools/

COPY docker-entrypoint.sh  /usr/local/bin/
RUN chmod u+x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 80

CMD ["node", "/ssdoz2sk-tools/serve.js"]

